import pymongo
import datetime

from bson.objectid import ObjectId

def get_db():
	client = pymongo.MongoClient('localhost', 27017)
	return client.flaskblog


def get_all():
	return list(get_db().posts.find())


def get_post(oid):
	return get_db().posts.find_one({'_id': ObjectId(oid)})


def get_tag(tag):
	return list(get_db().posts.find({'tags': tag})
		.sort("_id", pymongo.DESCENDING))

def get_tag_offset(tag, skip, limit):
	return list(get_db().posts.find({'tags': tag}, skip=skip, limit=limit)
		.sort("_id", pymongo.DESCENDING))

def entry_count_tag(tag):
	return get_db().posts.count_documents({'tags': tag})


def insert(title, author, mdcontent, tags, date = datetime.datetime.utcnow()):

	get_db().posts.insert({
		"title" : title,
		"author" : author,
		"mdcontent" : mdcontent,
		"tags" : tags,
		"date" : date
	})


def delete(oid):
	get_db().posts.delete_one({'_id': ObjectId(oid)})


def get_offset(skip, limit):
	return list(get_db().posts.find(skip=skip, limit=limit)
		.sort("_id", pymongo.DESCENDING))


def entry_count():
	return get_db().posts.count_documents({})