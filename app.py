from flask import Flask, render_template, redirect, \
	request, abort, flash, session

import markdown2

import json
import math
from functools import wraps

import entries

app = Flask(__name__)
app.secret_key = "Something unique and secret"
config = {}


def load_config():
	"""
	Return config dict loaded from json file
	"""
	with open('config.json') as f:
		return json.load(f)


def cskip(page):
	"""
	Return the number of entries needed to skip to display page
	"""
	if page <= 1:
		return 0
	else:
		return config['page_size']*(page-1)


def page_count(tag=None):
	"""
	Return page count, with optional argument for tag search
	"""
	if tag:
		return math.ceil(entries.entry_count_tag(tag) / config['page_size'])
	else:
		return math.ceil(entries.entry_count() / config['page_size'])


def preprocess_entries(entries):
	"""
	Return preprocessed entries for display
	"""
	for e in entries:

		trimmed = [str(s.strip() + "\n\n")  for s in e['mdcontent'].split("\n")]

		e['oid'] = str(e['_id'])
		e["preview"] = markdown2.markdown("".join(trimmed[:4]))
		e['date'] = e['date'].strftime('%A %-I:%M %p, %-d %B %Y')

	return entries


@app.route('/')
def v_index():
	"""
	Start page view function
	"""
	ent = preprocess_entries(entries.get_offset(0,config['page_size']))

	return render_template('index.html', 
		entries=ent, 
		current_page = 1,
		num_pages = page_count()
		)


@app.route('/p/<int:page>')
def v_page(page):
	"""
	Display a given page
	"""
	ent = preprocess_entries(
		entries.get_offset(cskip(page),config['page_size']))

	return render_template('index.html', 
		entries=ent, 
		current_page = page,
		num_pages = page_count()
		)


@app.route('/t/<tag>')
def v_tag(tag):
	"""
	Display first page of tag search
	"""
	ent = preprocess_entries(entries.get_tag(tag))

	return render_template('tag.html', 
		entries=ent,
		tag = tag,
		current_page = 1,
		num_pages = page_count(tag)
		)


@app.route('/t/<tag>/<int:page>')
def v_tag_page(tag, page):
	"""
	Display further tag search pages
	"""
	npages = page_count(tag)

	if page > npages:
		abort(404)

	skip = cskip(page)
	offset = config['page_size']
	
	ent = preprocess_entries(entries.get_tag_offset(tag, skip, offset))

	return render_template('tag.html',
		tag=tag,
		entries=ent,
		current_page = page,
		num_pages = page_count(tag)
		)

@app.route('/e/<oid>')
def v_post(oid):
	"""
	Display single entry
	"""
	entry = entries.get_post(oid)
	entry['html'] = markdown2.markdown(entry['mdcontent'])
	entry['date'] = entry['date'].strftime('%A %-I:%M %p, %-d %B %Y')

	return render_template('entry.html', entry=entry)


## ADMIN INTERFACE FUNCTIONS


def login_required(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		try:
			logged = session['logged']
		except KeyError:
			abort(403)
		return f(*args, **kwargs)

	return decorated_function


@app.route('/a/login', methods=['GET', 'POST'])
def a_login():
	"""
	Admin login page
	"""
	if request.method == 'GET':
		return render_template('admin/login.html')


	usern = config['admin']['user']
	passw = config['admin']['pass']

	if request.form['u'] == usern and request.form['p'] == passw:
		session['logged'] = True

		return redirect('/a/landing')
	else:
		return "Incorrect login",403


@app.route('/a/logout')
def a_logout():
	try:
		if session['logged']:
			session.pop('logged', None)
			return redirect('/')
	except KeyError:
		return "Not logged"

@app.route('/a/landing')
@login_required
def a_landing():
	"""
	Admin interface landing page
	"""
	return render_template("admin/landing.html")


@app.route('/a/entry_mgr')
@login_required
def a_entrymgr():
	"""
	Maintainer page for blog entries
	"""
	ent = preprocess_entries(entries.get_all())

	return render_template('admin/entry_mgr.html',
	 	entries=ent
	)


@app.route('/a/new_entry', methods=["POST"])
@login_required
def a_new_entry():
	"""
	New entry form endpoint
	"""
	entries.insert(request.form['title'], request.form['author'],
		request.form['mdcontent'], request.form['tags'].split(" "))

	flash("Entry Created")
	return redirect("/a/entry_mgr")


@app.route('/a/delete', methods=["POST"])
@login_required
def a_delete_entry():
	"""
	Delete entry form endpoint
	"""
	entries.delete(request.form['oid'])

	flash("Entry deleted")
	return redirect("/a/entry_mgr")


if __name__ == '__main__':

	config = load_config()
	app.jinja_env.globals['BLOG_NAME'] = config['blog_name']

	app.run(debug=True)

